package logica;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo {

	private ArrayList<HashSet<Integer>> listaDeVertices;

	public Grafo(int cantDeVertices) {
		cantidadDeVerticesValida(cantDeVertices);

		listaDeVertices = new ArrayList<HashSet<Integer>>();
		for (int verticescreados = 0; verticescreados < cantDeVertices; verticescreados++)
			listaDeVertices.add(new HashSet<Integer>());
	}

	public void agregarArista(int verticeA, int verticeB) {
		verticeValido(verticeA);
		verticeValido(verticeB);
		verificarDistintos(verticeA, verticeB);

		listaDeVertices.get(verticeA).add(verticeB);
		listaDeVertices.get(verticeB).add(verticeA);
	}

	public void eliminarArista(int verticeA, int verticeB) {
		verticeValido(verticeA);
		verticeValido(verticeB);
		verificarDistintos(verticeA, verticeB);

		listaDeVertices.get(verticeA).remove(verticeB);
		listaDeVertices.get(verticeB).remove(verticeA);
	}

	public boolean existeArista(int verticeA, int verticeB) {
		verticeValido(verticeA);
		verticeValido(verticeB);
		verificarDistintos(verticeA, verticeB);

		return listaDeVertices.get(verticeA).contains(verticeB);
	}

	public int tamanoDeGrafo() {
		return listaDeVertices.size();
	}

	public int cantidadAristas() {
		return tamanoDeGrafo() - 1;
	}

	public Set<Integer> vecinos(int verticeA) {
		verticeValido(verticeA);

		return listaDeVertices.get(verticeA);
	}

	public int gradoDeVertice(int verticeA) {
		return vecinos(verticeA).size();
	}

	public int verticeDeMayorGrado(Set<Integer> todosLosVertices) {
		int gradoMayor = 0;
		int verticeMayor = 0;

		for (int vertice : todosLosVertices) {
			if (gradoDeVertice(vertice) == 0) {
				return vertice;
			}
			if (gradoMayor < gradoDeVertice(vertice)) {
				gradoMayor = gradoDeVertice(vertice);
				verticeMayor = vertice;
			}
		}
		return verticeMayor;
	}

	public Set<Integer> conjuntoDeVertices() {
		Set<Integer> vertices = new HashSet<Integer>();

		for (int i = 0; i < tamanoDeGrafo(); i++) {
			vertices.add(i);
		}
		return vertices;
	}

	private void cantidadDeVerticesValida(int cantVertices) {
		if (cantVertices < 0)
			throw new IllegalArgumentException("El grafo no puede tener cantidad negativa de vertices");
	}

	private void verticeValido(int i) {

		if (i < 0)
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);

		if (i >= tamanoDeGrafo())
			throw new IllegalArgumentException("Los vertices deben estar entre 1 y |v|-1: " + i);
	}

	private void verificarDistintos(int i, int j) {
		if (i == j)
			throw new IllegalArgumentException("No se permiten loops : " + "(" + i + "," + j + ")");
	}
}