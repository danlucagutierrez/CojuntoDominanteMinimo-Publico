package logica;

import java.util.HashSet;

public class Vertice {
	private int verticeNro;
	private HashSet<Integer> vecinos;

	public Vertice(int v) {	
		verticeNro = v;
		vecinos = new HashSet<Integer>();
	}

	public void agregarVecino(int verticeVecino) {
		vecinos.add(verticeVecino);
	}

	public HashSet<Integer> getVecinosVertice() {
		return vecinos;
	}

	public Integer getNumeroVertice() {
		return verticeNro;
	}
}