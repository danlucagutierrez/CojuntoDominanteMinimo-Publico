package logica;

import java.util.HashSet;
import java.util.Set;

public class Solver {

	private Grafo _grafo;

	public Solver(Grafo grafo) {
		_grafo = grafo;
	}

	// Complejidad = O(n^3 + n^2 + n)
	public Set<Integer> conjuntoDominanteMinimo() {
		Set<Integer> totalVertices = _grafo.conjuntoDeVertices();
		Set<Integer> CDM = new HashSet<Integer>();
		Set<Integer> verticesDominados;

		while (totalVertices.size() != 0) {
			int verticeMayorGrado = _grafo.verticeDeMayorGrado(totalVertices);
			CDM.add(verticeMayorGrado);
			verticesDominados = _grafo.vecinos(verticeMayorGrado);
			verticesDominados.add(verticeMayorGrado);
			totalVertices.removeAll(verticesDominados);
		}
		return CDM;
	}
}