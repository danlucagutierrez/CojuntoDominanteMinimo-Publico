package interfaz;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import logica.Grafo;
import logica.Vertice;

public class GrafoJSON {
	private int cantidadDeVertices;
	private ArrayList<Vertice> vecinosDeVertices;

	public GrafoJSON(int cantidad, ArrayList<Vertice> lista) {
		cantidadDeVertices = cantidad;
		vecinosDeVertices = lista;
	}

	public String generarJSON() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);

		return json;
	}

	public void guardarJSON(String jsonParaGuardar, String archivoDestino) {
		try {
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(jsonParaGuardar);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static GrafoJSON leerJSON(String archivo) {
		Gson gson = new Gson();
		GrafoJSON ret = null;

		try {
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, GrafoJSON.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static Grafo crearGrafo(GrafoJSON json) {

		Grafo grafo = new Grafo(json.cantidadDeVertices);

		for (Vertice vertice : json.vecinosDeVertices) {

			for (Integer vecino : vertice.getVecinosVertice()) {
				grafo.agregarArista(vertice.getNumeroVertice(), vecino);
			}
		}
		return grafo;
	}
}