package interfaz;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import logica.Grafo;
import logica.Solver;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.Set;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.TextArea;

public class GrafoSWING {

	private JFrame _frameCDM;
	private Grafo _grafo;
	private String _aristasAgregadas = "";
	private boolean _bandera = false;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GrafoSWING window = new GrafoSWING();
					window._frameCDM.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GrafoSWING() {
		initialize();
	}

	private void initialize() {
		_frameCDM = new JFrame();
		_frameCDM.setResizable(false);
		_frameCDM.getContentPane().setBackground(SystemColor.controlHighlight);
		_frameCDM.setTitle("Conjunto Dominante Minimo");
		_frameCDM.setBounds(100, 100, 780, 480);
		_frameCDM.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_frameCDM.getContentPane().setLayout(null);
		_frameCDM.setLocationRelativeTo(null);

		JLabel labelGeneradorDeCDM = new JLabel("Generador de Conjunto Dominante Minimo");
		labelGeneradorDeCDM.setForeground(Color.DARK_GRAY);
		labelGeneradorDeCDM.setHorizontalAlignment(SwingConstants.CENTER);
		labelGeneradorDeCDM.setFont(new Font("Consolas", Font.BOLD, 30));
		labelGeneradorDeCDM.setBounds(10, 11, 754, 67);
		_frameCDM.getContentPane().add(labelGeneradorDeCDM);

		JLabel labelInsertarVertices = new JLabel("Insertar vertices");
		labelInsertarVertices.setBounds(20, 89, 221, 36);
		labelInsertarVertices.setForeground(Color.DARK_GRAY);
		labelInsertarVertices.setHorizontalAlignment(SwingConstants.LEFT);
		labelInsertarVertices.setFont(new Font("Consolas", Font.PLAIN, 24));
		_frameCDM.getContentPane().add(labelInsertarVertices);

		JLabel labelCantidadVertices = new JLabel("Cantidad de vertices");
		labelCantidadVertices.setForeground(Color.DARK_GRAY);
		labelCantidadVertices.setHorizontalAlignment(SwingConstants.LEFT);
		labelCantidadVertices.setFont(new Font("Consolas", Font.PLAIN, 12));
		labelCantidadVertices.setBounds(30, 128, 140, 22);
		_frameCDM.getContentPane().add(labelCantidadVertices);

		JSpinner spinnerCantidadVertices = new JSpinner(new SpinnerNumberModel(2, 2, 10000, 1));
		spinnerCantidadVertices.setFont(new Font("Consolas", Font.PLAIN, 12));
		spinnerCantidadVertices.setBounds(190, 126, 41, 22);
		_frameCDM.getContentPane().add(spinnerCantidadVertices);

		JLabel labelInsertarAristas = new JLabel("Insertar aristas");
		labelInsertarAristas.setHorizontalAlignment(SwingConstants.LEFT);
		labelInsertarAristas.setForeground(Color.DARK_GRAY);
		labelInsertarAristas.setFont(new Font("Consolas", Font.PLAIN, 24));
		labelInsertarAristas.setBounds(20, 250, 211, 41);
		_frameCDM.getContentPane().add(labelInsertarAristas);

		JLabel labelArista = new JLabel("Arista entre vertices");
		labelArista.setHorizontalAlignment(SwingConstants.LEFT);
		labelArista.setForeground(Color.DARK_GRAY);
		labelArista.setBounds(30, 296, 150, 45);
		labelArista.setFont(new Font("Consolas", Font.PLAIN, 12));
		_frameCDM.getContentPane().add(labelArista);

		JSpinner spinnerVerticeA = new JSpinner(new SpinnerNumberModel(0, 0, 10000, 1));
		spinnerVerticeA.setBounds(190, 296, 41, 20);
		spinnerVerticeA.setFont(new Font("Consolas", Font.PLAIN, 12));
		_frameCDM.getContentPane().add(spinnerVerticeA);

		JSpinner spinnerVerticeB = new JSpinner(new SpinnerNumberModel(0, 0, 10000, 1));
		spinnerVerticeB.setBounds(190, 321, 41, 20);
		spinnerVerticeB.setFont(new Font("Consolas", Font.PLAIN, 12));
		_frameCDM.getContentPane().add(spinnerVerticeB);

		JLabel labelMensajeError = new JLabel("�Error en datos ingresados!");
		labelMensajeError.setForeground(Color.RED);
		labelMensajeError.setBackground(Color.WHITE);
		labelMensajeError.setHorizontalAlignment(SwingConstants.LEFT);
		labelMensajeError.setBounds(267, 214, 231, 22);
		labelMensajeError.setFont(new Font("Consolas", Font.PLAIN, 14));
		_frameCDM.getContentPane().add(labelMensajeError);
		labelMensajeError.setVisible(false);

		JLabel labelMensajeAlerta = new JLabel("\u00A1Cuidado, primero debes ingresar vertices!");
		labelMensajeAlerta.setHorizontalAlignment(SwingConstants.LEFT);
		labelMensajeAlerta.setForeground(Color.ORANGE);
		labelMensajeAlerta.setFont(new Font("Consolas", Font.PLAIN, 14));
		labelMensajeAlerta.setBounds(267, 212, 336, 26);
		_frameCDM.getContentPane().add(labelMensajeAlerta);
		labelMensajeAlerta.setVisible(false);

		JLabel labelAristasAgregadas = new JLabel("Aristas agregadas");
		labelAristasAgregadas.setHorizontalAlignment(SwingConstants.RIGHT);
		labelAristasAgregadas.setForeground(Color.BLUE);
		labelAristasAgregadas.setFont(new Font("Consolas", Font.PLAIN, 14));
		labelAristasAgregadas.setBounds(267, 99, 480, 26);
		_frameCDM.getContentPane().add(labelAristasAgregadas);

		TextArea textAreaAristasAgregadas = new TextArea();
		textAreaAristasAgregadas.setEditable(false);
		textAreaAristasAgregadas.setBounds(267, 128, 480, 78);
		textAreaAristasAgregadas.setFont(new Font("Consolas", Font.PLAIN, 12));
		_frameCDM.getContentPane().add(textAreaAristasAgregadas);

		JButton botonAgregarVertices = new JButton("Agregar vertices");
		botonAgregarVertices.setForeground(Color.DARK_GRAY);
		botonAgregarVertices.setFont(new Font("Consolas", Font.PLAIN, 12));
		_frameCDM.getContentPane().add(botonAgregarVertices);

		botonAgregarVertices.setFont(new Font("Consolas", Font.PLAIN, 12));
		botonAgregarVertices.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Integer cantidadVertices = Integer.parseInt(spinnerCantidadVertices.getValue().toString());
				_grafo = new Grafo(cantidadVertices);
				botonAgregarVertices.setVisible(false);
				_bandera = true;
				labelMensajeAlerta.setVisible(false);
			}
		});

		botonAgregarVertices.setBounds(30, 161, 200, 45);
		_frameCDM.getContentPane().add(botonAgregarVertices);

		JLabel labelCDM = new JLabel("Conjunto Dominante Minimo");
		labelCDM.setHorizontalAlignment(SwingConstants.RIGHT);
		labelCDM.setForeground(Color.BLUE);
		labelCDM.setFont(new Font("Consolas", Font.PLAIN, 14));
		labelCDM.setBounds(267, 266, 480, 26);
		_frameCDM.getContentPane().add(labelCDM);

		TextArea textAreaCDM = new TextArea();
		textAreaCDM.setEditable(false);
		textAreaCDM.setBounds(267, 296, 480, 101);
		textAreaCDM.setFont(new Font("Consolas", Font.PLAIN, 12));
		_frameCDM.getContentPane().add(textAreaCDM);

		JButton botonCrearCDM = new JButton("Crear CDM");
		botonCrearCDM.setForeground(Color.DARK_GRAY);
		botonCrearCDM.setBounds(267, 247, 200, 45);
		botonCrearCDM.setFont(new Font("Consolas", Font.PLAIN, 12));
		_frameCDM.getContentPane().add(botonCrearCDM);
		botonCrearCDM.setVisible(false);

		botonCrearCDM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Solver solucion = new Solver(_grafo);
				Set<Integer> CDM = solucion.conjuntoDominanteMinimo();
				textAreaCDM.setText("Un Conjunto Dominante Minimo posible es: " + CDM.toString());
			}
		});

		JButton botonAgregarAristas = new JButton("Agregar arista");
		botonAgregarAristas.setForeground(Color.DARK_GRAY);
		botonAgregarAristas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Integer verticeA = Integer.parseInt(spinnerVerticeA.getValue().toString());
				Integer verticeB = Integer.parseInt(spinnerVerticeB.getValue().toString());

				spinnerVerticeA.setValue(0);
				spinnerVerticeB.setValue(0);

				if (!_bandera)
					labelMensajeAlerta.setVisible(true);
				else {
					try {
						_grafo.agregarArista(verticeA, verticeB);
						_aristasAgregadas = _aristasAgregadas + "\nArista agregadas entre los vertices: " + "("
								+ verticeA + ")" + " - " + "(" + verticeB + ")";
						textAreaAristasAgregadas.setText(_aristasAgregadas);
						labelMensajeError.setVisible(false);
						botonCrearCDM.setVisible(true);
					} catch (Exception RuntimeException) {
						labelMensajeError.setVisible(true);
					}
				}
			}
		});
		botonAgregarAristas.setFont(new Font("Consolas", Font.PLAIN, 12));
		botonAgregarAristas.setBounds(30, 352, 200, 45);
		_frameCDM.getContentPane().add(botonAgregarAristas);
	}
}