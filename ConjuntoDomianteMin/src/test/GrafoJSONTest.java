package test;

import java.util.ArrayList;
import org.junit.Test;
import interfaz.GrafoJSON;
import logica.Grafo;
import logica.Vertice;

public class GrafoJSONTest {

	@Test
	public void grafoValidoTest() {
		Grafo grafoPrueba = grafoValido();

		int[] esperados = { 0, 1, 2 };

		Assert.iguales(esperados, grafoPrueba.conjuntoDeVertices());
	}

	@Test
	public void vecinosValidoTest() {
		Grafo grafoPrueba = grafoValido();

		int[] vecinosDe0 = { 1, 2 };
		int[] vecinosDe1 = { 0, 2 };
		int[] vecinosDe2 = { 0, 1 };

		Assert.iguales(vecinosDe0, grafoPrueba.vecinos(0));
		Assert.iguales(vecinosDe1, grafoPrueba.vecinos(1));
		Assert.iguales(vecinosDe2, grafoPrueba.vecinos(2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticesNegativosTest() {

		grafoVerticesNegativos();
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticesFueraDeRangoTest() {

		grafoVerticesFueraDeRango();
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticesIgualesTest() {

		grafoVerticesIguales();
	}

	@Test(expected = IllegalArgumentException.class)
	public void cantidadVerticesNegativaTest() {

		grafoCantidadVerticesNegativa();
	}

	private Grafo grafoValido() {
		ArrayList<Vertice> listaDeVertices = new ArrayList<Vertice>();

		Vertice vertice0 = new Vertice(0);
		vertice0.agregarVecino(1);
		vertice0.agregarVecino(2);

		Vertice vertice1 = new Vertice(1);
		vertice1.agregarVecino(0);
		vertice1.agregarVecino(2);

		Vertice vertice2 = new Vertice(2);
		vertice2.agregarVecino(0);
		vertice2.agregarVecino(1);

		listaDeVertices.add(vertice0);
		listaDeVertices.add(vertice1);
		listaDeVertices.add(vertice2);

		int cantidadDeVertices = 3;

		return crearGrafo(cantidadDeVertices, listaDeVertices);
	}

	private Grafo grafoVerticesNegativos() {
		ArrayList<Vertice> listaDeVertices = new ArrayList<Vertice>();

		Vertice vertice0 = new Vertice(0);
		vertice0.agregarVecino(-1);
		vertice0.agregarVecino(-2);

		Vertice vertice1 = new Vertice(-1);
		vertice1.agregarVecino(0);
		vertice1.agregarVecino(-2);

		Vertice vertice2 = new Vertice(-2);
		vertice2.agregarVecino(0);
		vertice2.agregarVecino(-1);

		listaDeVertices.add(vertice0);
		listaDeVertices.add(vertice1);
		listaDeVertices.add(vertice2);

		int cantidadDeVertices = 3;

		return crearGrafo(cantidadDeVertices, listaDeVertices);
	}

	private Grafo grafoVerticesFueraDeRango() {
		ArrayList<Vertice> listaDeVertices = new ArrayList<Vertice>();

		Vertice vertice0 = new Vertice(0);
		vertice0.agregarVecino(1);
		vertice0.agregarVecino(2);

		Vertice vertice1 = new Vertice(1);
		vertice1.agregarVecino(0);
		vertice1.agregarVecino(2);

		Vertice vertice2 = new Vertice(2);
		vertice2.agregarVecino(0);
		vertice2.agregarVecino(1);

		listaDeVertices.add(vertice0);
		listaDeVertices.add(vertice1);
		listaDeVertices.add(vertice2);

		int cantidadDeVertices = 2;

		return crearGrafo(cantidadDeVertices, listaDeVertices);
	}

	private Grafo grafoVerticesIguales() {
		ArrayList<Vertice> listaDeVertices = new ArrayList<Vertice>();

		Vertice vertice0 = new Vertice(0);
		vertice0.agregarVecino(0);

		listaDeVertices.add(vertice0);

		int cantidadDeVertices = 1;

		return crearGrafo(cantidadDeVertices, listaDeVertices);
	}

	private Grafo grafoCantidadVerticesNegativa() {
		ArrayList<Vertice> listaDeVertices = new ArrayList<Vertice>();

		Vertice vertice0 = new Vertice(0);
		vertice0.agregarVecino(1);

		listaDeVertices.add(vertice0);

		int cantidadDeVertices = -5;

		return crearGrafo(cantidadDeVertices, listaDeVertices);
	}

	private Grafo crearGrafo(int cantidad, ArrayList<Vertice> vertices) {
		GrafoJSON grafoJSONcreado = new GrafoJSON(cantidad, vertices);
		String grafoJSONPretty = grafoJSONcreado.generarJSON();
		grafoJSONcreado.guardarJSON(grafoJSONPretty, "GrafoPretty.JSON");

		GrafoJSON grafoJSONleido = GrafoJSON.leerJSON("GrafoPretty.JSON");
		Grafo grafoCreadoDesdeJSONleido = GrafoJSON.crearGrafo(grafoJSONleido);

		return grafoCreadoDesdeJSONleido;
	}
}