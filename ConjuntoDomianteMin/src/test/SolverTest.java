package test;

import org.junit.Test;
import logica.Grafo;
import logica.Solver;

public class SolverTest {

	@Test
	public void esUnCDMTest() {

		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(2, 4);

		Solver solucion = new Solver(grafo);
		int[] esperados = { 0, 2 };

		Assert.iguales(esperados, solucion.conjuntoDominanteMinimo());
	}

	@Test
	public void noEsUnCDMTest() {

		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(2, 4);

		Solver solucion = new Solver(grafo);
		int[] esperados = { 1, 4 };

		Assert.distintos(esperados, solucion.conjuntoDominanteMinimo());
	}

	@Test
	public void unElementoEnCDMTest() {

		Grafo grafo = new Grafo(3);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(1, 2);

		Solver solucion = new Solver(grafo);

		int[] esperados = { 1 };

		Assert.iguales(esperados, solucion.conjuntoDominanteMinimo());
	}

	@Test
	public void unCDMposibleTest() {

		Grafo grafo = new Grafo(8);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(2, 5);
		grafo.agregarArista(4, 3);
		grafo.agregarArista(4, 5);
		grafo.agregarArista(4, 6);
		grafo.agregarArista(6, 7);

		Solver solucion = new Solver(grafo);

		int[] esperados = { 0, 2, 4, 7 };
		// El conjunto ideal es { 1, 4, 7 } pero estamos trabajando con un algoritmo
		// goloso

		Assert.iguales(esperados, solucion.conjuntoDominanteMinimo());
	}
}