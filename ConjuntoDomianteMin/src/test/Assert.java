package test;

import static org.junit.Assert.*;
import java.util.Set;

public class Assert {

	public static void iguales(int[] esperados, Set<Integer> obtenido) {

		assertEquals(esperados.length, obtenido.size());

		for (int i = 0; i < esperados.length; i++) {
			assertTrue(obtenido.contains(esperados[i]));
		}
	}

	public static void distintos(int[] esperados, Set<Integer> obtenido) {

		assertEquals(esperados.length, obtenido.size());

		for (int i = 0; i < esperados.length; i++) {
			assertFalse(obtenido.contains(esperados[i]));
		}
	}
}